import sqlite3 as sql
from flask import Flask, render_template, request
import logging
import sys

root = logging.getLogger()
root.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
root.addHandler(ch)

conn=sql.connect('eat_it.db')
app=Flask(__name__)

@app.route('/test2', methods=['GET'])
def test():
    good= request.args.get('good', None)
    conn.row_factory = sql.Row
    c= conn.cursor()
    c.execute("SELECT name , calories ,fats FROM goods WHERE name == ['good']")
    data=c.fetchall()
    return render_template("test2.html")
    app.logger.error(e)


if __name__ == "__main__":
    app.run(port=1000,debug=True)
